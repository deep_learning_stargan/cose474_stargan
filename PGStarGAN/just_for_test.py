import torch
from PGStarGAN.model import Generator, Discriminator

model = Discriminator()


model.grow_model()
model.flush_model()
model.grow_model()
model.flush_model()
print(model)
print(model(torch.randn([1, 3, 512, 512])))

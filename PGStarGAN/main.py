import os
import argparse
from solver import Solver
from torch.backends import cudnn
from data_loader import get_dataset


def str2bool(v):
    return v.lower() in ('true')

def main(config):
    # For fast training.
    cudnn.benchmark = True

    # Create directories if not exist.
    if not os.path.exists(config.log_dir):
        os.makedirs(config.log_dir)
    if not os.path.exists(config.model_save_dir):
        os.makedirs(config.model_save_dir)
    if not os.path.exists(config.sample_dir):
        os.makedirs(config.sample_dir)
    if not os.path.exists(config.result_dir):
        os.makedirs(config.result_dir)

    dataset = get_dataset(config.celeba_image_dir[0], config.attr_path,
                          config.selected_attrs, config.image_size, mode=config.mode)

    # Solver for training and testing StarGAN.
    solver = Solver(dataset, config)

    if config.mode == 'train':
        solver.train()
    elif config.mode == 'test':
        solver.test()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    # Model configuration.
    parser.add_argument('--c_dim', type=int, default=5, help='dimension of domain labels')
    parser.add_argument('--image_size', type=int, default=128, help='initial image resolution')
    parser.add_argument('--g_conv_dim', type=int, default=64, help='number of conv filters in the first layer of G')
    parser.add_argument('--d_conv_dim', type=int, default=64, help='number of conv filters in the first layer of D')
    parser.add_argument('--g_repeat_num', type=int, default=6, help='number of residual blocks in G')
    parser.add_argument('--d_repeat_num', type=int, default=6, help='number of strided conv layers in D')
    parser.add_argument('--lambda_cls', type=float, default=1, help='weight for domain classification loss')
    parser.add_argument('--lambda_rec', type=float, default=10, help='weight for reconstruction loss')
    parser.add_argument('--lambda_gp', type=float, default=10, help='weight for gradient penalty')

    # Training configuration.
    parser.add_argument('--batch_size', type=int, nargs='+', default=[12, 7, 4], help='mini-batch size')
    parser.add_argument('--num_iters', type=int, default=285000, help='number of total iterations for training D')
    parser.add_argument('--num_iters_decay', type=int, default=100000, help='number of iterations for decaying lr')
    parser.add_argument('--g_lr', type=float, default=2e-5, help='learning rate for G')
    parser.add_argument('--d_lr', type=float, default=3e-5, help='learning rate for D')
    parser.add_argument('--n_critic', type=int, default=12, help='number of D updates per each G update')
    parser.add_argument('--beta1', type=float, default=0.5, help='beta1 for Adam optimizer')
    parser.add_argument('--beta2', type=float, default=0.999, help='beta2 for Adam optimizer')
    parser.add_argument('--resume_iters', type=int, default=None, help='resume training from this step')

    # Test configuration.
    parser.add_argument('--test_iters', type=int, default=285000, help='test model from this step')

    # Miscellaneous.
    parser.add_argument('--num_workers', type=int, default=2)
    parser.add_argument('--mode', type=str, default='train', choices=['train', 'test'])
    parser.add_argument('--use_tensorboard', type=str2bool, default=True)

    parser.add_argument('--image_info', type=str, default='./dataset_info.json')

    # Directories.
    parser.add_argument('--log_dir', type=str, default='D:/stargan/logs')
    parser.add_argument('--model_save_dir', type=str, default='D:/stargan/models')
    parser.add_argument('--sample_dir', type=str, default='D:/stargan/samples')
    parser.add_argument('--result_dir', type=str, default='D:/stargan/results')
    parser.add_argument('--celeba_image_dir', nargs='+', type=str, default=['D:/Download/celeba-hq/celeba-128', 'D:/Download/celeba-hq/celeba-256', 'D:/Download/celeba-hq/celeba-512'])
    parser.add_argument('--attr_path', type=str, default='D:/Download/celeba/Anno/list_attr_celeba.txt')
    parser.add_argument('--selected_attrs', '--list', nargs='+', help='selected attributes for the CelebA dataset',
                        default=['Black_Hair', 'Blond_Hair', 'Brown_Hair', 'Male', 'Young'])

    # Step size.
    parser.add_argument('--log_step', type=int, default=10)
    parser.add_argument('--sample_step', type=int, default=100)
    parser.add_argument('--model_save_step', type=int, default=2000)
    parser.add_argument('--lr_update_step', type=int, default=750)

    parser.add_argument('--fade_image_iter', type=int, default=600000)
    parser.add_argument('--stab_image_iter', type=int, default=600000)

    parser.add_argument('--initial_resolution_status', type=float, default=0)

    parser.add_argument('--max_expansion', type=int, default=2)

    parser.add_argument('--lrelu_slope', type=float, default=0.2)

    config = parser.parse_args()
    print(config)
    main(config)

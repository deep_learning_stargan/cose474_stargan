import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from model_utility import deepcopy_module, soft_copy_param, get_module_names


class FadeInLayer(nn.Module):
    def __init__(self):
        super(FadeInLayer, self).__init__()
        self.alpha = 0.0

    def update_alpha(self, delta):
        self.alpha = self.alpha + delta
        self.alpha = max(0, min(self.alpha, 1.0))

    # input : [x_low, x_high] from ConcatTable()
    def forward(self, x):
        return torch.add(x[0].mul(1.0 - self.alpha), x[1].mul(self.alpha))


class ConcatTable(nn.Module):
    def __init__(self, layer1, layer2):
        super(ConcatTable, self).__init__()
        self.layer1 = layer1
        self.layer2 = layer2

    def forward(self, x):
        y = [self.layer1(x), self.layer2(x)]
        return y


class ResidualBlock(nn.Module):
    """Residual Block with instance normalization."""

    def __init__(self, dim_in, dim_out, lrelu_slope=0.2):
        super(ResidualBlock, self).__init__()
        self.main = nn.Sequential(
            nn.Conv2d(dim_in, dim_out, kernel_size=3, stride=1, padding=1, bias=False),
            nn.InstanceNorm2d(dim_out, affine=True, track_running_stats=True),
            nn.LeakyReLU(negative_slope=lrelu_slope, inplace=True),
            nn.Conv2d(dim_out, dim_out, kernel_size=3, stride=1, padding=1, bias=False),
            nn.InstanceNorm2d(dim_out, affine=True, track_running_stats=True)
        )

    def forward(self, x):
        return x + self.main(x)


class ResBlk(nn.Module):
    def __init__(self, dim_in, dim_out):
        super(ResBlk, self).__init__()

    def forward(self, *input):
        pass


class Generator(nn.Module):
    """Generator network."""

    def __init__(self, device, conv_dim=64, c_dim=5, repeat_num=5, lrelu_slope=0.2):
        super(Generator, self).__init__()

        self.c_dim = c_dim
        self.conv_dim = conv_dim
        self.main = nn.Sequential()
        self.device = device

        self.front_fade = None
        self.back_fade = None

        self.next_block_cnt = 0

        self.stab_able = False
        self.lrelu_slope = lrelu_slope

        layers = []
        self.main.add_module('from_rgb_block', self.from_rgb(conv_dim))
        layers.append(nn.Conv2d(conv_dim, conv_dim, kernel_size=3, stride=1, padding=1, bias=False))
        layers.append(nn.InstanceNorm2d(conv_dim, affine=True, track_running_stats=True))
        layers.append(nn.LeakyReLU(negative_slope=self.lrelu_slope, inplace=True))

        # Down-sampling layers.
        curr_dim = conv_dim
        for i in range(2):
            layers.append(nn.AvgPool2d(kernel_size=2, stride=2))
            layers.append(nn.Conv2d(curr_dim, curr_dim * 2, kernel_size=3, stride=1, padding=1, bias=False))
            layers.append(nn.InstanceNorm2d(curr_dim * 2, affine=True, track_running_stats=True))
            layers.append(nn.LeakyReLU(negative_slope=self.lrelu_slope, inplace=True))
            curr_dim = curr_dim * 2

        # Bottleneck layers.
        for i in range(repeat_num):
            layers.append(ResidualBlock(dim_in=curr_dim, dim_out=curr_dim))

        # Up-sampling layers.
        for i in range(2):
            layers.append(nn.UpsamplingNearest2d(scale_factor=2))
            layers.append(nn.Conv2d(curr_dim, curr_dim // 2, kernel_size=3, stride=1, padding=1, bias=False))
            layers.append(nn.InstanceNorm2d(curr_dim // 2, affine=True, track_running_stats=True))
            layers.append(nn.LeakyReLU(negative_slope=self.lrelu_slope, inplace=True))
            curr_dim = curr_dim // 2

        layers.append(nn.Conv2d(curr_dim, curr_dim, kernel_size=3, stride=1, padding=1, bias=False))
        layers.append(nn.InstanceNorm2d(curr_dim, affine=True, track_running_stats=True))
        layers.append(nn.LeakyReLU(negative_slope=self.lrelu_slope, inplace=True))

        self.main.add_module('init_stargan', nn.Sequential(*layers))
        self.main.add_module('to_rgb_block', self.to_rgb(conv_dim))

    def forward(self, x, c):
        c = c.view(c.size(0), c.size(1), 1, 1)
        c = c.repeat(1, 1, x.size(2), x.size(3))
        x = torch.cat([x, c], dim=1)
        return self.main(x)

    def from_rgb(self, dim_out):
        rgb_module = nn.Sequential(
            nn.Conv2d(self.c_dim + 3, dim_out, kernel_size=1),
            nn.LeakyReLU(negative_slope=self.lrelu_slope, inplace=True)
        )
        return rgb_module

    def to_rgb(self, dim_in):
        rgb_module = nn.Sequential(
            nn.Conv2d(dim_in, 3, kernel_size=1)
        )
        return rgb_module

    def intermediate_block(self, dim, is_up):
        layers = []

        if is_up:
            layers.append(nn.Upsample(mode='nearest', scale_factor=2))
            layers.append(nn.Conv2d(dim, dim // 2, kernel_size=3, stride=1, padding=1, bias=False))
            layers.append(nn.InstanceNorm2d(dim // 2, affine=True, track_running_stats=True))
            layers.append(nn.LeakyReLU(negative_slope=self.lrelu_slope, inplace=True))
            layers.append(nn.Conv2d(dim // 2, dim // 2, kernel_size=3, stride=1, padding=1, bias=False))
            layers.append(nn.InstanceNorm2d(dim // 2, affine=True, track_running_stats=True))
            layers.append(nn.LeakyReLU(negative_slope=self.lrelu_slope, inplace=True))

        else:
            layers.append(nn.Conv2d(dim // 2, dim // 2, kernel_size=3, stride=2, padding=1, bias=False))
            layers.append(nn.InstanceNorm2d(dim // 2, affine=True, track_running_stats=True))
            layers.append(nn.LeakyReLU(negative_slope=self.lrelu_slope, inplace=True))
            layers.append(nn.Conv2d(dim // 2, dim, kernel_size=3, stride=1, padding=1, bias=False))
            layers.append(nn.InstanceNorm2d(dim, affine=True, track_running_stats=True))
            layers.append(nn.LeakyReLU(negative_slope=self.lrelu_slope, inplace=True))

        return nn.Sequential(*layers)

    def grow_model(self):
        if self.stab_able:
            print(f'Generator grew model while not available timing {self.next_block_cnt}')
            return
        print(f'Model is growing from {32 * 256 // self.conv_dim} to {64 * 256 // self.conv_dim}')
        self.stab_able = True
        new_model = nn.Sequential()
        # ?욌뮘濡??ㅼ썙以섏빞??

        previous_from_rgb = deepcopy_module(self.main, 'from_rgb_block')
        front_previous_block = nn.Sequential()
        front_previous_block.add_module('previous_down_sample', nn.AvgPool2d(kernel_size=2))
        front_previous_block.add_module('previous_from_rgb', previous_from_rgb)

        intermediate_block_downsample = self.intermediate_block(is_up=False, dim=self.conv_dim)

        front_next_block = nn.Sequential()
        front_next_block.add_module('next_from_rgb', self.from_rgb(self.conv_dim // 2))
        front_next_block.add_module(f'front_next_block_{self.next_block_cnt}', intermediate_block_downsample)

        new_model.add_module('front_concat_block', ConcatTable(front_previous_block, front_next_block))

        self.front_fade = FadeInLayer()
        new_model.add_module('front_fadein_block', self.front_fade)

        for name, module in self.main.named_children():
            if name != 'to_rgb_block' and name != 'from_rgb_block':
                new_model.add_module(name, module)  # make new structure and,
                new_model[-1].load_state_dict(module.state_dict())

        intermediate_block_upsample = self.intermediate_block(is_up=True, dim=self.conv_dim)

        back_previous_to_rgb = deepcopy_module(self.main, 'to_rgb_block')
        back_previous_block = nn.Sequential()
        back_previous_block.add_module('previous_up_sample', nn.Upsample(scale_factor=2, mode='nearest'))
        back_previous_block.add_module('previous_to_rgb', back_previous_to_rgb)

        back_next_block = nn.Sequential()
        back_next_block.add_module(f'back_next_block_{self.next_block_cnt}', intermediate_block_upsample)
        back_next_block.add_module('next_to_rgb', self.to_rgb(self.conv_dim // 2))

        new_model.add_module('back_concat_block', ConcatTable(back_previous_block, back_next_block))
        self.back_fade = FadeInLayer()
        new_model.add_module('back_fadein_block', self.back_fade)

        del self.main

        self.main = new_model.to(self.device)
        self.conv_dim //= 2

    def flush_model(self):
        if not self.stab_able:
            print(f'Generator flush model while not available timing {self.next_block_cnt}')
            return
        print('flushing network... It may take few seconds...')
        # make deep copy and paste.
        front_next_block_name = f'front_next_block_{self.next_block_cnt}'
        next_block = deepcopy_module(self.main.front_concat_block.layer2, front_next_block_name)
        next_from_rgb = deepcopy_module(self.main.front_concat_block.layer2, 'next_from_rgb')

        # add the high resolution block.
        new_model = nn.Sequential()
        new_model.add_module('from_rgb_block', next_from_rgb)
        new_model.add_module(front_next_block_name, next_block)

        # add rest.
        for name, module in self.main.named_children():
            print(name)
            if name != 'front_concat_block' and name != 'front_fadein_block' \
                    and name != 'back_concat_block' and name != 'back_fadein_block':
                new_model.add_module(name, module)  # make new structure and,
                new_model[-1].load_state_dict(module.state_dict())  # copy pretrained weights

        back_next_block_name = f'back_next_block_{self.next_block_cnt}'

        next_block = deepcopy_module(self.main.back_concat_block.layer2, back_next_block_name)
        next_to_rgb = deepcopy_module(self.main.back_concat_block.layer2, 'next_to_rgb')

        new_model.add_module(back_next_block_name, next_block)
        new_model.add_module('to_rgb_block', next_to_rgb)

        self.front_fade = None
        self.back_fade = None

        del self.main

        self.main = new_model.to(self.device)
        self.next_block_cnt += 1

        self.stab_able = False

    def update_alpha(self, delta):
        if self.front_fade is not None:
            self.front_fade.update_alpha(delta)
            self.back_fade.update_alpha(delta)
        else:
            print('fade is none but tried updating in Generator')


class Discriminator(nn.Module):
    """Discriminator network with PatchGAN."""

    def __init__(self, device, image_size=128, conv_dim=64, c_dim=5, repeat_num=6, lrelu_slope=0.2):
        super(Discriminator, self).__init__()
        self.main = nn.Sequential()
        self.lrelu_slope = lrelu_slope
        layers = []
        self.conv_dim = conv_dim
        self.main.add_module('from_rgb_block', self.from_rgb(conv_dim))
        self.stab_able = False
        self.next_block_cnt = 0
        self.device = device

        curr_dim = conv_dim
        layers.append(nn.Conv2d(curr_dim, curr_dim, kernel_size=3, stride=1, padding=1))
        layers.append(nn.LeakyReLU(self.lrelu_slope))
        layers.append(nn.AvgPool2d(kernel_size=2, stride=2))

        for i in range(1, repeat_num):
            layers.append(nn.Conv2d(curr_dim, curr_dim * 2, kernel_size=3, stride=1, padding=1))
            layers.append(nn.LeakyReLU(self.lrelu_slope))
            layers.append(nn.AvgPool2d(kernel_size=2, stride=2))
            curr_dim *= 2

        kernel_size = int(image_size / np.power(2, repeat_num))
        self.main.add_module('init_dis', nn.Sequential(*layers))
        self.conv1 = nn.Conv2d(curr_dim, 1, kernel_size=3, stride=1, padding=1, bias=False)
        self.conv2 = nn.Conv2d(curr_dim, c_dim, kernel_size=kernel_size, bias=False)

        self.fade = None

    def forward(self, x):
        h = self.main(x)
        out_src = self.conv1(h)
        out_cls = self.conv2(h)
        return out_src, out_cls.view(out_cls.size(0), out_cls.size(1))

    def intermediate_block(self, dim):
        layers = []
        layers.append(nn.Conv2d(dim // 2, dim // 2, kernel_size=3, stride=1, padding=1, bias=False))
        layers.append(nn.InstanceNorm2d(dim // 2, affine=True, track_running_stats=True))
        layers.append(nn.LeakyReLU(negative_slope=self.lrelu_slope, inplace=True))
        layers.append(nn.Conv2d(dim // 2, dim, kernel_size=3, stride=1, padding=1, bias=False))
        layers.append(nn.InstanceNorm2d(dim, affine=True, track_running_stats=True))
        layers.append(nn.LeakyReLU(negative_slope=self.lrelu_slope, inplace=True))
        layers.append(nn.AvgPool2d(kernel_size=2, stride=2))
        return nn.Sequential(*layers)

    def grow_model(self):
        if self.stab_able:
            print(f'Discriminator grew model while not available timing {self.next_block_cnt}')
            return
        previous_from_rgb = deepcopy_module(self.main, 'from_rgb_block')
        front_previous_block = nn.Sequential()
        front_previous_block.add_module('previous_down_sample', nn.AvgPool2d(kernel_size=2))
        front_previous_block.add_module('previous_from_rgb', previous_from_rgb)

        intermediate_block_downsample = self.intermediate_block(dim=self.conv_dim)

        front_next_block = nn.Sequential()
        front_next_block.add_module('next_from_rgb', self.from_rgb(self.conv_dim // 2))
        front_next_block.add_module(f'next_block_{self.next_block_cnt}', intermediate_block_downsample)

        new_model = nn.Sequential()
        new_model.add_module('concat_block', ConcatTable(front_previous_block, front_next_block))
        self.fade = FadeInLayer()
        new_model.add_module('fadein_block', self.fade)

        for name, module in self.main.named_children():
            if not name == 'from_rgb_block':
                new_model.add_module(name, module)  # make new structure and,
                new_model[-1].load_state_dict(module.state_dict())  # copy pretrained weights

        del self.main

        self.main = new_model.to(self.device)
        self.conv_dim //= 2

        self.stab_able = True

    def from_rgb(self, dim_out):
        rgb_module = nn.Sequential(
            nn.Conv2d(3, dim_out, kernel_size=1),
            nn.LeakyReLU(self.lrelu_slope)
        )
        return rgb_module

    def flush_model(self):
        if not self.stab_able:
            print(f'Discriminator flush model while not available timing {self.next_block_cnt}')
            return
        print('flushing network... It may take few seconds...')
        # make deep copy and paste.
        next_block_name = f'next_block_{self.next_block_cnt}'
        front_next_block = deepcopy_module(self.main.concat_block.layer2, next_block_name)
        front_next_from_rgb = deepcopy_module(self.main.concat_block.layer2, 'next_from_rgb')

        # add the high resolution block.
        new_model = nn.Sequential()
        new_model.add_module('from_rgb_block', front_next_from_rgb)
        new_model.add_module(next_block_name, front_next_block)

        # add rest.
        for name, module in self.main.named_children():
            print(name)
            if name != 'concat_block' and name != 'fadein_block':
                new_model.add_module(name, module)  # make new structure and,
                new_model[-1].load_state_dict(module.state_dict())  # copy pretrained weights
        self.fade = None

        self.main = new_model
        self.next_block_cnt += 1

        self.stab_able = False

    def update_alpha(self, delta):
        if self.fade is not None:
            self.fade.update_alpha(delta)
        else:
            print('Fade is none but tried to update')


# TODO: PGGAN Stable Phase Coding

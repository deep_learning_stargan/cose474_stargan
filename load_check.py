import torch
from model import Generator


loaded_file = torch.load('./stargan/models/200000-G.ckpt', map_location='cpu')
model = Generator()
model.load_state_dict(loaded_file)

print(model)
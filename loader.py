import json
import glob

from PIL import Image
import torch
from torchvision import transforms
from torch.utils.data import Dataset, DataLoader


class PosterDataset(Dataset):
    def __init__(self, path_info: dict, transform, image_size):
        self.attr2idx = {}
        self.idx2attr = {}
        self.transform = transform
        self.image_size = image_size
        self.datalist = []
        for cnt, (key, val) in enumerate(path_info.items()):
            self.attr2idx[key] = cnt
            self.idx2attr[cnt] = key

            for image_folder in val:
                img_list = glob.glob(image_folder + "/*.jpg")
                for img_name in img_list:
                    self.datalist.append((img_name, cnt))

    def __getitem__(self, item):
        img_name, label = self.datalist[item]
        try:
            image = Image.open(img_name)
            image = image.convert('RGB')
            image = self.transform(image)
        except:
            image = torch.zeros([3, *self.image_size])
        return image, label

    def __len__(self):
        return len(self.datalist)


def get_loader(config):
    path_info = json.load(open(config.image_info, 'r'))
    if config.mode == 'train':
        path_info = path_info['train']
    else:
        path_info = path_info['test']
    transform_list = []

    if config.mode == 'train':
        transform_list.append(transforms.RandomHorizontalFlip())

    transform_list.append(transforms.Resize([config.image_size, config.image_size]))
    transform_list.append(transforms.ToTensor())
    transform_list.append(transforms.Normalize(mean=(0.5, 0.5, 0.5), std=(0.5, 0.5, 0.5)))
    transform = transforms.Compose(transform_list)

    poster_dataset = PosterDataset(path_info, transform, [config.image_size, config.image_size])
    result_data_loader = DataLoader(poster_dataset, config.batch_size, shuffle=True, num_workers=config.num_workers)
    return result_data_loader
